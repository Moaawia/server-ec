const express = require("express");
const router = express.Router();
const fs = require("fs");

router.get("/", (req, res) => {
  const user = req.user;

  if (!user) {
    return res.status(200).json([]);
  }

  fs.readFile("./db.json", "utf8", (err, data) => {
    if (err) {
      console.error("Error reading db.json:", err);
      return res.status(500).json({ error: "Internal Server Error" });
    }

    const fileData = JSON.parse(data);
    return res
      .status(200)
      .json(fileData.cartItems.filter((item) => item.userId == user.id));
    next();
  });
});

module.exports = router;

// const express = require("express");
// const jsonrouter = require("./server");
// const router = express.Router();
// // const jsonServer = require("json-server");

// // const jsonrouter = jsonServer.router("db.json");

// router.get("/", (req, res) => {
//   const user = req.user;

//   if (!user) {
//     return res.status(200).json([]);
//   }

//   // const jsonrouter = jsonServer.router("db.json");

//   const cartItems = jsonrouter.db.get("cartItems").value();

//   return res
//     .status(200)
//     .json(cartItems.filter((item) => item.userId == user.id));
//   next();
// });

// module.exports = router;
