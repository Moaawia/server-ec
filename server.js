const jsonServer = require("json-server");
const usersRoute = require("./usersRoute");
const cartRoute = require("./cartRoute");
const { log } = require("console");
const jwt = require("jsonwebtoken");
const multer = require("multer");
const express = require("express");
const bcrypt = require("bcrypt");

// const upload = multer({ dest: "images/" });

const server = jsonServer.create();
const jsonrouter = jsonServer.router("db.json");
const middlewares = jsonServer.defaults();
const cors = require("cors");
const corsOptions = {
  origin: "http://localhost:3000",
  credentials: true, //access-control-allow-credentials:true
  optionSuccessStatus: 200,
};
server.use(cors(corsOptions));
// server.use("/static", express.static("public"));
// const path = require("path");z

const SECRET_KEY = "n#3sG2E*%y@8H$6fT5^d!qW&aU+mP1jS0";

// Middleware to read user data from db.json on each request
server.use((req, res, next) => {
  req.users = jsonrouter.db.get("users").value();
  console.log(req.users.length);
  next();
});

// Custom middleware to check token validity
server.use((req, res, next) => {
  const token = req.headers.authorization;
  console.log(token);
  if (token) {
    jwt.verify(token.slice("Bearer ".length), SECRET_KEY, (err, user) => {
      // if (err) {
      //   return res.status(401).json({ error: "Invalid token" });
      // }

      if (user) req.user = req.users.find((u) => u.id === user.id);
      console.log(user);
      next();
    });
  } else {
    next();
    // res.status(401).json({ error: "Token required" });
  }
});

server.use(jsonServer.bodyParser);

const storage = multer.diskStorage({
  destination: "public/",
  filename: function (req, file, cb) {
    // Set the desired filename
    const uniqueSuffix = Date.now() + "-" + Math.round(Math.random() * 1e9);
    const extension = file.originalname.split(".").pop();
    const fileName = `${uniqueSuffix}.${extension}`;
    cb(null, fileName);
  },
});
const upload = multer({ storage: storage });

server.use(upload.single("image"));

// Custom route handler for handling image uploads
server.post("/images", (req, res) => {
  // Access the uploaded file via req.file
  // You can save the file or perform any desired processing here

  // Send a response indicating the upload was successful
  res.json({
    message: "File uploaded successfully",
    imageName: req.file.filename,
  });
});

server.use(middlewares);

// Hash the password before saving it
server.post("/users", (req, res) => {
  const saltRounds = 10; // Number of salt rounds
  const { email, password } = req.body;
  const user = req.users.find((u) => u.email === email);
  if (user) return res.status(400).json({ message: "Email already exists" });

  bcrypt.hash(password, saltRounds, (err, hash) => {
    if (err) {
      return res.status(500).json({ message: "Error hashing password" });
    }
    req.body.password = hash;

    const maxId = req.users.reduce(
      (max, item) => (item.id > max ? item.id : max),
      0
    );
    req.body.id = maxId + 1;

    const newusers = jsonrouter.db.get("users").push(req.body).write();
    const { password, ...userInfo } = newusers[newusers.length - 1];
    const token = jwt.sign(
      { id: userInfo.id, email: userInfo.email },
      SECRET_KEY
    );
    return res.json({ ...userInfo, token });
  });
});

server.delete("/cartItems", (req, res) => {
  console.log("A");
  jsonrouter.db.get("cartItems").remove().write();
  console.log("A");

  return res.status(200).json();
});

server.use("/users", usersRoute);
server.use("/cartItems", cartRoute);

server.use(jsonrouter);

server.listen(8000, () => {
  console.log("JSON Server is running on port 8000");
});
