const express = require("express");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const router = express.Router();

const SECRET_KEY = "n#3sG2E*%y@8H$6fT5^d!qW&aU+mP1jS0";

router.post("/login", (req, res) => {
  const { email, password } = req.body;
  const user = req.users.find((u) => u.email === email);
  if (!user) {
    return res.status(401).json({
      message: "Can't find account for " + email + "email address",
    });
  }

  bcrypt.compare(password, user.password, (err, result) => {
    if (err) {
      return res.status(401).json({ message: "Authentication failed" });
    }

    if (result) {
      const token = jwt.sign({ id: user.id, email: user.email }, SECRET_KEY);
      const { password, ...userInfo } = user;
      return res.json({ ...userInfo, token });
    } else {
      return res.status(401).json({ message: "Incorrect password" });
    }
  });
});

// Custom route handler for POST request
// server.post("/items", (req, res, next) => {
//   const newItem = req.body;
//   // Customize the response as needed
//   newItem.id = Date.now(); // Assign a unique ID
//   // Send the customized response
//   res.json(newItem);
// });

module.exports = router;
